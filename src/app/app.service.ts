import {Injectable} from '@angular/core';
import {TodoItem} from './todo/todoItem';
import {TodoList} from './todo/todoList';

@Injectable({
  providedIn: 'root',
})

export class AppService {

  todoLists: TodoList[] = [
    new TodoList({id: 1, title: 'Work'}),
    new TodoList({id: 2, title: 'Home'}),
    new TodoList({id: 3, title: 'Groceries'})
  ];
  LOCALSTORAGE_KEY = 'todoList';

  constructor() {
    this.LoadTodoLists();
  }

  addTodo(todo: TodoItem, listId: number): AppService {
    const list = this.todoLists.find(e => e.id === listId);
    todo.id =  this.findNextId();
    list.todos.unshift(todo);
    list.newItem = new TodoItem();
    this.saveTodoLists();
    return this;
  }

  deleteTodoById(id: number): AppService {
    this.todoLists.forEach(list => {
      list.todos = list.todos
      .filter(todo => todo.id !== id);
    });
    this.saveTodoLists();
    return this;
  }

  findNextId(): number {
    let nextId = 0;
    this.todoLists.forEach(list => {
      const max = Math.max.apply(Math, list.todos.map(o => o.id));
      if (max > nextId) {
        nextId = max;
      }
    });
    return ++nextId;
  }

  getTodoLists(): TodoList[] {
    this.findNextId();
    return this.todoLists;
  }

  saveTodoLists() {
    localStorage.setItem(this.LOCALSTORAGE_KEY, JSON.stringify(this.todoLists));
  }

  LoadTodoLists() {
    const lists = JSON.parse(localStorage.getItem(this.LOCALSTORAGE_KEY));
    if (lists != null && lists.length === 3) {
      this.todoLists = lists;
    }
  }
}

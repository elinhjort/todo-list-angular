import {TodoItem} from './todoItem';

export class TodoList {
  id: number;
  title = 'New list';
  todos: TodoItem[] = [];
  newItem: TodoItem = new TodoItem();

  constructor(values: object = {}) {
    Object.assign(this, values);
  }
}

import {Component} from '@angular/core';
import {AppService} from './app.service';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import { TodoList } from './todo/todoList';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [AppService]
})

export class AppComponent {

  constructor(private appService: AppService) {
  }

  addTodo(list: TodoList) {
    this.appService.addTodo(list.newItem, list.id);
  }

  saveTodoLists() {
    this.appService.saveTodoLists();
  }

  removeTodo(todo) {
    this.appService.deleteTodoById(todo.id);
  }

  get todoLists() {
    return this.appService.getTodoLists();
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);

    }
    this.appService.saveTodoLists();
  }
}
